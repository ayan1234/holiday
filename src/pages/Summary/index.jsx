import { useContext } from 'react'
import { UserContext } from 'contexts'
import Styles from './styles.module.scss';

const Summary = () => {
  const { state } = useContext(UserContext);

  return (
    <div className={Styles.wrapper}>
      <p>Name: { state.name }</p>
      <p>Preference: { state.preference }</p>
      <p>States : { state.states.join(',') }</p>
      <p>Suggestions: { state.suggestion } </p>
    </div>
  )
};

export default Summary;