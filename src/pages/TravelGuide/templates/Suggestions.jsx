import { useState, useContext } from "react";
import Form from 'components/Form';
import { UserContext } from 'contexts/index';

const Suggestions = ({ onCancel, onSubmit }) => {
  const { state, dispatch } = useContext(UserContext);
  const [suggestion, setSuggestion] = useState(state.suggestion);

  const handleChange = (event) => {
    const { value } = event.target;
    setSuggestion(value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!suggestion) {
      dispatch({ type: 'error', payload: 'Please fill the form' });
      return;
    }
    dispatch({ type: 'field', payload: { suggestion } })
    onSubmit();
  }

  return (
    <Form onSubmit={handleSubmit} onCancel={onCancel}>
      <label>Suggestions</label>
      <textarea
        name='suggestion'
        placeholder='Any suggestions for us?'
        rows={5}
        cols={50}
        onChange={handleChange}
        type='textarea'
        value={suggestion}
      />
      {state.error && <p className='error'>{ state.error }</p>}
    </Form>
  )
};

export default Suggestions;