import { useContext, useState } from 'react';
import Form from 'components/Form';
import Input from 'components/Input';
import { UserContext } from 'contexts/index';

const Username = ({ onSubmit, onCancel }) => {
  const { state, dispatch } = useContext(UserContext);
  const [name, setName] = useState(state.name);

  const handleChange = (event) => {
    const { value } = event.target;
    setName(value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!name) {
      dispatch({ type: 'error', payload: 'Please fill your name' });
      return;
    };
    dispatch({ type: 'field', payload: { name } });
    onSubmit();
  }

  return (
    <Form onSubmit={handleSubmit} onCancel={onCancel}>
      <label>Name</label>
      <Input
        name='name'
        type='text'
        onChange={handleChange}
        placeholder='Type your name'
        value={name}
      />
      {state.error && <p className='error'>{ state.error }</p>}
    </Form>
  );
}

export default Username;