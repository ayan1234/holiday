import { useState, useContext } from 'react';
import { BEACH, MOUNTAIN } from 'constants/index';
import { UserContext } from 'contexts/index';
import Checkbox from 'components/Checkbox';
import Form from 'components/Form'

const BeachStates = [
  'Goa',
  'Pondicherry',
  'Kerala',
];

const MountainStates = [
  'Shimla',
  'Manali',
  'Nainital',
];

const SelectCity = ({ onCancel, onSubmit }) => {
  const { dispatch, state } = useContext(UserContext);
  const [selectedStates, setSelectedStates] = useState(state.states);

  const handleChange = (event) => {
    const { name } = event.target;
    if (selectedStates.includes(name)) {
      setSelectedStates(selectedStates.filter(state => state !== name));
    }
    else {
      setSelectedStates([...selectedStates, name]);
    }
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (selectedStates.length === 0) {
      dispatch({ type: 'error', payload: 'Please select one state' });
      return;
    }
    dispatch({ type: 'field', payload: { states: selectedStates } })
    onSubmit();
  }

  return (
    <Form onSubmit={handleSubmit} onCancel={onCancel}>
      <label>Select where you want to go?</label>
      {state.preference === BEACH &&
        BeachStates.map((state, index) => (
          <Checkbox
            key={index}
            name={state}
            label={state}
            isChecked={selectedStates.includes(state)}
            onChange={handleChange}
          />
        ))
      }
      {state.preference === MOUNTAIN &&
        MountainStates.map((state, index) => (
          <Checkbox
            key={index}
            name={state}
            label={state}
            isChecked={selectedStates.includes(state)}
            onChange={handleChange}
          />
        ))
      }
      {state.error && <p className='error'>{ state.error }</p>}
    </Form>
  );
};

export default SelectCity;