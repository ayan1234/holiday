import { useState, useContext } from 'react';
import { BEACH, MOUNTAIN } from 'constants/index';
import { UserContext } from 'contexts/index';
import Form from 'components/Form';

const Preference = ({ onCancel, onSubmit }) => {
  const { state, dispatch } = useContext(UserContext);
  const [selected, setSelected] = useState(state.preference);

  const handleChange = (event) => {
    setSelected(event.currentTarget.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!selected) {
      dispatch({ type: 'error', payload: 'Please select your preference' });
      return;
    }
    dispatch({ type: 'field', payload: { preference: selected } })
    onSubmit();
  }

  return (
    <Form onSubmit={handleSubmit} onCancel={onCancel}>
      <label>What do you prefer?</label>
      <div>
        <input
          type='radio'
          name='preference'
          onChange={handleChange}
          checked={selected === BEACH}
          value={BEACH}
        />
        { BEACH }
        <input
          type='radio'
          name='preference'
          onChange={handleChange}
          checked={selected === MOUNTAIN}
          value={MOUNTAIN}
        />
        { MOUNTAIN }
      </div>
      {state.error && <p className='error'>{ state.error }</p>}
    </Form>
  )
}

export default Preference;