import { useState } from "react";
import { Redirect } from 'react-router-dom';
import Username from './templates/Username';
import Preference from './templates/Preference';
import SelectCity from './templates/SelectCity';
import Suggestions from './templates/Suggestions';
import Styles from './styles.module.scss';

const TravelGuide = () => {
  const [currentStep, setCurrentStep] = useState(0);
  const [redirect, setRedirect] = useState(false);

  const renderForm = () => {
    switch(currentStep) {
      case 0:
        return <Username onSubmit={handleSubmit} onCancel={goToPreviousStep} />;
      case 1:
        return <Preference onSubmit={handleSubmit} onCancel={goToPreviousStep} />;
      case 2:
        return <SelectCity onSubmit={handleSubmit} onCancel={goToPreviousStep} />
      case 3:
        return <Suggestions onSubmit={handleSubmit} onCancel={goToPreviousStep} />
      default:
        return null;

    }
  }

  const goToNextStep = () => {
    setCurrentStep(step => step + 1);
  }

  const goToPreviousStep = () => {
    if (currentStep === 0) return;
    setCurrentStep(step => step - 1);
  }

  const handleSubmit = (event) => {
    if (currentStep < 3) {
      goToNextStep();
      return;
    }

    setRedirect(true);
  }

  if (redirect) {
    return (
      <Redirect to='/summary' />
    );
  }

  return (
    <div className={Styles.wrapper}>
      {renderForm()}
    </div>
  )
};

export default TravelGuide;