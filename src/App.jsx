import { useReducer } from 'react';
import { Switch, Route } from 'react-router-dom';
import { UserContext } from 'contexts/index';
import TravelGuide from 'pages/TravelGuide';
import Summary from 'pages/Summary';
import './App.css';

const initialState = {
  name: '',
  states: [],
  preference: null,
  suggestions: '',
  error: null,
}

const formReducer = (state, action) => {
  switch (action.type) {
    case 'field':
      return {
        ...state,
        ...action.payload,
        error: '',
      };
    case 'error':
      return {
        ...state,
        error: action.payload,
      }
    default:
      return state;
  }
}


function App() {
  const [state, dispatch] = useReducer(formReducer, initialState);
  return (
    <UserContext.Provider value={{ state, dispatch }}>
      <Switch>
        <Route path='/summary' component={Summary} />
        <Route path='/' component={TravelGuide} exact />
      </Switch>
    </UserContext.Provider>
  );
}

export default App;
