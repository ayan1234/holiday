const Input = ({ onChange, name, value, type, placeholder }) => (
  <input
    className={StyleSheet.input}
    name={name}
    placeholder={placeholder}
    type={type}
    onChange={onChange}
    value={value}
  />
);

export default Input;