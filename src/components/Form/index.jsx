import Button from 'components/Button';
import Styles from './styles.module.scss';

const Form = ({ children, onSubmit, onCancel }) => {
  return (
    <form
      className={Styles.form}
      onSubmit={onSubmit}>
      { children }
      <div className={Styles.actionBar}>
        <Button
          classname={Styles.rightMargin}
          label='Back'
          onClick={onCancel}
        />
        <Button
          type='submit'
          label='Next'
        />
      </div>
    </form>
  );
};

export default Form;