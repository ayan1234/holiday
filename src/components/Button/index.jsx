import PropTypes from 'prop-types';
import Styles from './styles.module.scss';

const Button = ({ type = 'button', classname, label, onClick }) => (
  <button
    className={`${Styles.btn} ${classname}`}
    onClick={onClick}
    type={type}
  >
    { label }
  </button>
);

Button.propTypes = {
  classname: PropTypes.string,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default Button;
