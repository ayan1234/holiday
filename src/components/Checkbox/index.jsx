const Checkbox = ({ label, isChecked, name, onChange }) => {
  return (
    <label>
      <input
        name={name}
        type='checkbox'
        checked={isChecked}
        onChange={onChange}
      />
      { label }
    </label>
  );
};

export default Checkbox;